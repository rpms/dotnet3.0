# Avoid provides/requires from private libraries
%global privlibs             libhostfxr
%global privlibs %{privlibs}|libclrjit
%global privlibs %{privlibs}|libcoreclr
%global privlibs %{privlibs}|libcoreclrtraceptprovider
%global privlibs %{privlibs}|libdbgshim
%global privlibs %{privlibs}|libhostpolicy
%global privlibs %{privlibs}|libmscordaccore
%global privlibs %{privlibs}|libmscordbi
%global privlibs %{privlibs}|libsos
%global privlibs %{privlibs}|libsosplugin
%global __provides_exclude ^(%{privlibs})\\.so
%global __requires_exclude ^(%{privlibs})\\.so

# Filter flags not supported by clang/dotnet:
#  -fstack-clash-protection is not supported by clang
#  -specs= is not supported by clang
%global dotnet_cflags %(echo %optflags | sed -e 's/-fstack-clash-protection//' | sed -re 's/-specs=[^ ]*//g')
%if 0%{?fedora} < 30
# on Fedora 29, clang, -fcf-protection and binutils interact in strage ways leading to
# "<corrupt x86 feature size: 0x8>" errors.
%global dotnet_cflags %(echo %dotnet_cflags | sed -e 's/ -fcf-protection//')
%endif
%global dotnet_ldflags %(echo %{__global_ldflags} | sed -re 's/-specs=[^ ]*//g')

%global host_version 3.0.3
%global runtime_version 3.0.3
%global aspnetcore_runtime_version %{runtime_version}
%global sdk_version 3.0.103
%global templates_version %(echo %{runtime_version} | awk 'BEGIN { FS="."; OFS="." } {print $1, $2, $3+1 }')

%global host_rpm_version %{host_version}
%global aspnetcore_runtime_rpm_version %{aspnetcore_runtime_version}
%global runtime_rpm_version %{runtime_version}
%global sdk_rpm_version %{sdk_version}

%if 0%{?fedora} || 0%{?rhel} < 8
%global use_bundled_libunwind 0
%else
%global use_bundled_libunwind 1
%endif

%if 0%{?fedora}
%global runtime_id fedora.%{fedora}-x64
%else
%if 0%{?centos}
%global runtime_id centos.%{centos}-x64
%else
%global runtime_id rhel.%{rhel}-x64
%endif
%endif

Name:           dotnet3.0
Version:        %{sdk_rpm_version}
Release:        1%{?dist}
Summary:        .NET Core CLI tools and runtime
License:        MIT and ASL 2.0 and BSD
URL:            https://github.com/dotnet/

# The source is generated on a Fedora box via:
# ./build-dotnet-tarball dotnet-v%%{runtime_version}
Source0:        dotnet-v%{sdk_version}-SDK.tar.gz
Source1:        check-debug-symbols.py
Source2:        dotnet.sh

Patch1:         shared-compilation.patch

Patch100:       corefx-optflags-support.patch

Patch300:       core-setup-do-not-strip.patch

Patch500:       cli-telemetry-optout.patch

ExclusiveArch:  x86_64

BuildRequires:  clang
BuildRequires:  cmake
# Bootstrap SDK needs OpenSSL 1.0 to run, but we can build and then
# run with either OpenSSL 1.0 or 1.1
%if 0%{?fedora} >= 26 || 0%{?rhel} >= 8
BuildRequires:  compat-openssl10
%endif
BuildRequires:  coreutils
BuildRequires:  git
%if 0%{?fedora} || 0%{?rhel} > 7
BuildRequires:  glibc-langpack-en
%endif
BuildRequires:  hostname
BuildRequires:  krb5-devel
BuildRequires:  libcurl-devel
BuildRequires:  libicu-devel
%if ! %{use_bundled_libunwind}
BuildRequires:  libunwind-devel
%endif
BuildRequires:  libuuid-devel
BuildRequires:  lldb-devel
BuildRequires:  llvm
BuildRequires:  lttng-ust-devel
BuildRequires:  openssl-devel
BuildRequires:  python3
BuildRequires:  strace
BuildRequires:  tar
BuildRequires:  zlib-devel

%description
.NET Core is a fast, lightweight and modular platform for creating
cross platform applications that work on Linux, macOS and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.

.NET Core contains a runtime conforming to .NET Standards a set of
framework libraries, an SDK containing compilers and a 'dotnet'
application to drive everything.


%package -n dotnet

Version:        %{sdk_rpm_version}
Summary:        .NET Core CLI tools and runtime

Requires:       dotnet-sdk-3.0%{?_isa} >= %{sdk_rpm_version}-%{release}

%description -n dotnet
.NET Core is a fast, lightweight and modular platform for creating
cross platform applications that work on Linux, macOS and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.

.NET Core contains a runtime conforming to .NET Standards a set of
framework libraries, an SDK containing compilers and a 'dotnet'
application to drive everything.


%package -n dotnet-host

Version:        %{host_rpm_version}
Summary:        .NET command line launcher

%description -n dotnet-host
The .NET Core host is a command line program that runs a standalone
.NET core application or launches the SDK.

.NET Core is a fast, lightweight and modular platform for creating
cross platform applications that work on Linux, Mac and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.


%package -n dotnet-hostfxr-3.0

Version:        %{host_rpm_version}
Summary:        .NET Core command line host resolver

# Theoretically any version of the host should work. But lets aim for the one
# provided by this package, or from a newer version of .NET Core
Requires:       dotnet-host%{?_isa} >= %{host_rpm_version}-%{release}

%description -n dotnet-hostfxr-3.0
The .NET Core host resolver contains the logic to resolve and select
the right version of the .NET Core SDK or runtime to use.

.NET Core is a fast, lightweight and modular platform for creating
cross platform applications that work on Linux, Mac and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.


%package -n dotnet-runtime-3.0

Version:        %{runtime_rpm_version}
Summary:        NET Core 3.0 runtime

Requires:       dotnet-hostfxr-3.0%{?_isa} >= %{host_rpm_version}-%{release}

# libicu is dlopen()ed
Requires:       libicu

%if %{use_bundled_libunwind}
Provides: bundled(libunwind) = 1.3
%endif

%description -n dotnet-runtime-3.0
The .NET Core runtime contains everything needed to run .NET Core applications.
It includes a high performance Virtual Machine as well as the framework
libraries used by .NET Core applications.

.NET Core is a fast, lightweight and modular platform for creating
cross platform applications that work on Linux, Mac and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.


%package -n aspnetcore-runtime-3.0

Version:        %{aspnetcore_runtime_rpm_version}
Summary:        ASP.NET Core 3.0 runtime

Requires:       dotnet-runtime-3.0%{?_isa} >= %{runtime_rpm_version}-%{release}

%description -n aspnetcore-runtime-3.0
The ASP.NET Core runtime contains everything needed to run .NET Core
web applications. It includes a high performance Virtual Machine as
well as the framework libraries used by .NET Core applications.

ASP.NET Core is a fast, lightweight and modular platform for creating
cross platform web applications that work on Linux, Mac and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.


%package -n dotnet-templates-3.0

Version:        %{sdk_rpm_version}
Summary:        .NET Core 3.0 templates

# Theoretically any version of the host should work. But lets aim for the one
# provided by this package, or from a newer version of .NET Core
Requires:       dotnet-host%{?_isa} >= %{host_rpm_version}-%{release}

%description -n dotnet-templates-3.0
This package contains templates used by the .NET Core SDK.

ASP.NET Core is a fast, lightweight and modular platform for creating
cross platform web applications that work on Linux, Mac and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.


%package -n dotnet-sdk-3.0

Version:        %{sdk_rpm_version}
Summary:        .NET Core 3.0 Software Development Kit

Requires:       dotnet-runtime-3.0%{?_isa} >= %{runtime_rpm_version}-%{release}
Requires:       aspnetcore-runtime-3.0%{?_isa} >= %{aspnetcore_runtime_rpm_version}-%{release}

Requires:       dotnet-apphost-pack-3.0%{?_isa} >= %{runtime_rpm_version}-%{release}
Requires:       dotnet-targeting-pack-3.0%{?_isa} >= %{runtime_rpm_version}-%{release}
Requires:       aspnetcore-targeting-pack-3.0%{?_isa} >= %{aspnetcore_runtime_rpm_version}-%{release}
Requires:       netstandard-targeting-pack-2.1%{?_isa} >= %{sdk_rpm_version}-%{release}

Requires:       dotnet-templates-3.0%{?_isa} >= %{sdk_rpm_version}-%{release}

%description -n dotnet-sdk-3.0
The .NET Core SDK is a collection of command line applications to
create, build, publish and run .NET Core applications.

.NET Core is a fast, lightweight and modular platform for creating
cross platform applications that work on Linux, Mac and Windows.

It particularly focuses on creating console applications, web
applications and micro-services.


%define dotnet_targeting_pack() %{expand:
%package -n %{1}

Version:        %{2}
Summary:        Targeting Pack for %{3} %{4}

Requires:       dotnet-host

%description -n %{1}
This package provides a targetting pack for %{3} %{4}
that allows developers to compile against and target %{3} %{4}
applications using the .NET Core SDK.

%files -n %{1}
%dir %{_libdir}/dotnet/packs
%{_libdir}/dotnet/packs/%{5}
}

%dotnet_targeting_pack dotnet-apphost-pack-3.0 %{runtime_rpm_version} Microsoft.NETCore.App 3.0 Microsoft.NETCore.App.Host.%{runtime_id}
%dotnet_targeting_pack dotnet-targeting-pack-3.0 %{runtime_rpm_version} Microsoft.NETCore.App 3.0 Microsoft.NETCore.App.Ref
%dotnet_targeting_pack aspnetcore-targeting-pack-3.0 %{aspnetcore_runtime_rpm_version} Microsoft.AspNetCore.App 3.0 Microsoft.AspNetCore.App.Ref
%dotnet_targeting_pack netstandard-targeting-pack-2.1 %{sdk_rpm_version} NETStandard.Library 2.1 NETStandard.Library.Ref


%prep
%setup -q -n dotnet-v%{sdk_version}-SDK

# Fix bad hardcoded path in build
sed -i 's|/usr/share/dotnet|%{_libdir}/dotnet|' src/core-setup.*/src/corehost/common/pal.unix.cpp

# Disable warnings
sed -i 's|skiptests|skiptests ignorewarnings|' repos/coreclr.proj

%patch1 -p1

pushd src/corefx.*
%patch100 -p1
popd

pushd src/coreclr.*
popd

pushd src/core-setup.*
%patch300 -p1
popd

pushd src/cli.*
%patch500 -p1
popd

# If CLR_CMAKE_USE_SYSTEM_LIBUNWIND=TRUE is misisng, add it back
grep CLR_CMAKE_USE_SYSTEM_LIBUNWIND repos/coreclr.proj || \
    sed -i 's|\$(BuildArguments) </BuildArguments>|$(BuildArguments) cmakeargs -DCLR_CMAKE_USE_SYSTEM_LIBUNWIND=TRUE</BuildArguments>|' repos/coreclr.proj

%if %{use_bundled_libunwind}
sed -i 's|-DCLR_CMAKE_USE_SYSTEM_LIBUNWIND=TRUE|-DCLR_CMAKE_USE_SYSTEM_LIBUNWIND=FALSE|' repos/coreclr.proj
%endif

cat source-build-info.txt

find -iname 'nuget.config' -exec echo {}: \; -exec cat {} \; -exec echo \;


%build
cat /etc/os-release

export CFLAGS="%{dotnet_cflags}"
export CXXFLAGS="%{dotnet_cflags}"
export LDFLAGS="%{dotnet_ldflags}"

VERBOSE=1 ./build.sh \
  /v:n \
  /p:LogVerbosity=n \
  /p:MinimalConsoleLogOutput=false \
  /p:ContinueOnPrebuiltBaselineError=true \



%install
install -dm 0755 %{buildroot}%{_libdir}/dotnet
ls bin/x64/Release
tar xf bin/x64/Release/dotnet-sdk-%{sdk_version}-%{runtime_id}.tar.gz -C %{buildroot}%{_libdir}/dotnet/

# Install managed symbols
tar xf bin/x64/Release/runtime/dotnet-runtime-symbols-%{runtime_version}-%{runtime_id}.tar.gz \
    -C %{buildroot}/%{_libdir}/dotnet/shared/Microsoft.NETCore.App/%{runtime_version}/

# Fix permissions on files
find %{buildroot}%{_libdir}/dotnet/ -type f -name '*.dll' -exec chmod -x {} \;
find %{buildroot}%{_libdir}/dotnet/ -type f -name '*.pdb' -exec chmod -x {} \;
find %{buildroot}%{_libdir}/dotnet/ -type f -name '*.props' -exec chmod -x {} \;
find %{buildroot}%{_libdir}/dotnet/ -type f -name '*.pubxml' -exec chmod -x {} \;
find %{buildroot}%{_libdir}/dotnet/ -type f -name '*.targets' -exec chmod -x {} \;
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.NETCore.App.Ref/3.0.0/data/PackageOverrides.txt
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.NETCore.App.Ref/3.0.0/data/FrameworkList.xml
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.NETCore.App.Ref/3.0.0/data/PlatformManifest.txt
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/ref/netcoreapp3.0/*.xml
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/data/PackageOverrides.txt
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/data/FrameworkList.xml
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/data/PlatformManifest.txt
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/Microsoft.AspNetCore.App.Ref/3.0.1/Debug/netstandard2.0/Microsoft.AspNetCore.App.Ref.assets.cache
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/obj/Microsoft.AspNetCore.App.Ref.csproj.nuget.cache
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/obj/Microsoft.AspNetCore.App.Ref.csproj.nuget.dgspec.json
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.AspNetCore.App.Ref/3.0.1/obj/project.assets.json
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/NETStandard.Library.Ref/2.1.0/data/PackageOverrides.txt
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/NETStandard.Library.Ref/2.1.0/data/FrameworkList.xml
chmod 0755 %{buildroot}/%{_libdir}/dotnet/sdk/%{sdk_version}/AppHostTemplate/apphost
chmod 0755 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.NETCore.App.Host.%{runtime_id}/%{runtime_version}/runtimes/%{runtime_id}/native/apphost
chmod 0755 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.NETCore.App.Host.%{runtime_id}/%{runtime_version}/runtimes/%{runtime_id}/native/libnethost.so
chmod 0644 %{buildroot}/%{_libdir}/dotnet/packs/Microsoft.NETCore.App.Host.%{runtime_id}/%{runtime_version}/runtimes/%{runtime_id}/native/nethost.h


# Add ~/.dotnet/tools to $PATH for all users
install -dm 0755 %{buildroot}%{_sysconfdir}/profile.d/
install %{SOURCE2} %{buildroot}%{_sysconfdir}/profile.d/

install -dm 0755 %{buildroot}/%{_datadir}/bash-completion/completions
# dynamic completion needs the file to be named the same as the base command
install src/cli.*/scripts/register-completions.bash %{buildroot}/%{_datadir}/bash-completion/completions/dotnet

# TODO: the zsh completion script needs to be ported to use #compdef
#install -dm 755 %%{buildroot}/%%{_datadir}/zsh/site-functions
#install src/cli/scripts/register-completions.zsh %%{buildroot}/%%{_datadir}/zsh/site-functions/_dotnet

install -dm 0755 %{buildroot}%{_bindir}
ln -s %{_libdir}/dotnet/dotnet %{buildroot}%{_bindir}/

install -dm 0755 %{buildroot}%{_mandir}/man1/
find -iname 'dotnet*.1' -type f -exec cp {} %{buildroot}%{_mandir}/man1/ \;

echo "%{_libdir}/dotnet" >> install_location
install -dm 0755 %{buildroot}%{_sysconfdir}/dotnet
install install_location %{buildroot}%{_sysconfdir}/dotnet/

# Check debug symbols in all elf objects. This is not in %%check
# because native binaries are stripped by rpm-build after %%install.
# So we need to do this check earlier.
echo "Testing build results for debug symbols..."
%{SOURCE1} -v %{buildroot}%{_libdir}/dotnet/


%check
%{buildroot}%{_libdir}/dotnet/dotnet --info


%files -n dotnet
# empty package useful for dependencies

%files -n dotnet-host
%dir %{_libdir}/dotnet
%{_libdir}/dotnet/dotnet
%dir %{_libdir}/dotnet/host
%dir %{_libdir}/dotnet/host/fxr
%{_bindir}/dotnet
%license %{_libdir}/dotnet/LICENSE.txt
%license %{_libdir}/dotnet/ThirdPartyNotices.txt
%doc %{_mandir}/man1/dotnet*.1.gz
%{_sysconfdir}/profile.d/dotnet.sh
%{_sysconfdir}/dotnet
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/dotnet

%files -n dotnet-hostfxr-3.0
%dir %{_libdir}/dotnet/host/fxr
%{_libdir}/dotnet/host/fxr/%{host_version}

%files -n dotnet-runtime-3.0
%dir %{_libdir}/dotnet/shared
%dir %{_libdir}/dotnet/shared/Microsoft.NETCore.App
%{_libdir}/dotnet/shared/Microsoft.NETCore.App/%{runtime_version}

%files -n aspnetcore-runtime-3.0
%dir %{_libdir}/dotnet/shared
%dir %{_libdir}/dotnet/shared/Microsoft.AspNetCore.App
%{_libdir}/dotnet/shared/Microsoft.AspNetCore.App/%{aspnetcore_runtime_version}

%files -n dotnet-templates-3.0
%dir %{_libdir}/dotnet/templates
%{_libdir}/dotnet/templates/%{templates_version}

%files -n dotnet-sdk-3.0
%dir %{_libdir}/dotnet/sdk
%{_libdir}/dotnet/sdk/%{sdk_version}
%dir %{_libdir}/dotnet/packs

%changelog
* Thu Mar 05 2020 Omair Majid <omajid@redhat.com> - 3.0.103-1
- Update to .NET Core Runtime 3.0.3 and SDK 3.0.103
- Resolves: RHBZ#1806956

* Wed Jan 15 2020 Omair Majid <omajid@redhat.com> - 3.0.102-2
- Fix prebuilts leaking into the final build
- Fix regressions in binary hardering
- Resolves: RHBZ#1788171

* Fri Jan 10 2020 Omair Majid <omajid@redhat.com> - 3.0.102-1
- Update to .NET Core Runtime 3.0.2 and SDK 3.0.102
- Resolves: RHBZ#1788171

* Fri Sep 27 2019 Omair Majid <omajid@redhat.com> - 3.0.100-1
- Update to .NET Core Runtime 3.0.0 and SDK 3.0.100
- Resolves: RHBZ#1711403

* Thu Sep 19 2019 Omair Majid <omajid@redhat.com> - 3.0.100-0.8.preview9
- Fix package descriptions for targeting packs
- Re-generate tarball to remove samples
- Resolves: RHBZ#1711403

* Mon Sep 16 2019 Omair Majid <omajid@redhat.com> - 3.0.100-0.7.preview9
- Do not put targeting pack files in mutliple packages
- Resolves: RHBZ#1711403

* Mon Sep 16 2019 Omair Majid <omajid@redhat.com> - 3.0.100-0.6.preview9
- Update source archive
- Resolves: RHBZ#1711403

* Mon Sep 16 2019 Omair Majid <omajid@redhat.com> - 3.0.100-0.5.preview9
- Fix package Require versions on dotnet-sdk-3.0
- Resolves: RHBZ#1711403

* Sun Sep 15 2019 Omair Majid <omajid@redhat.com> - 3.0.100-0.4.preview9
- Update to .NET Core 3.0 Preview 9
- Resolves: RHBZ#1711403

* Fri Aug 09 2019 Omair Majid <omajid@redhat.com> - 3.0.100-0.3.preview7
- Initial import into RHEL 8
- Resolves: RHBZ#1711403
